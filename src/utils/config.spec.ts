import { expect } from 'chai'
import { config } from './config'

export default () => {
    describe('Envs config testing', () => {
        it('should load envs', () => {
            expect(config.env).not.to.be.a('null')
            expect(config.env).to.be.a('string')
            expect(config.rootDir).not.to.be.a('null')
            expect(config.rootDir).to.be.a('string')
            expect(config.data).not.to.be.a('undefined')
            expect(config.data)
                .and.haveOwnProperty('HOST')
                .to.be.a('string')
        })
    })
}

