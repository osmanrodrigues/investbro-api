import dotenv, { DotenvParseOutput } from 'dotenv'

class ConfigSingleton {
    constructor(
        public env = process.env.NODE_ENV,
        public rootDir = process.env.PWD,
        public data: DotenvParseOutput | undefined = undefined
    ) {
        this.data = dotenv.config({
            path: `${this.rootDir}/.env.${this.env}`,
        }).parsed
    }
}

export const config = new ConfigSingleton()
