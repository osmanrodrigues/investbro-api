
import { salutation } from '@adapters/constants'
import { config } from '@utils/config'
import Koa from 'koa'

const ENV = config.data
const app = new Koa()

app.use(async ctx => {
    ctx.body = { salutation }
})

app.listen(Number(ENV?.PORT), ENV?.HOST, undefined, () => {
    console.log(`App running at http://${ENV?.HOST}:${ENV?.PORT}`)
})
