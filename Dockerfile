# syntax=docker/dockerfile:1

FROM node:16.13-alpine
WORKDIR /api
COPY package.json .env.production ./
COPY dist ./dist
RUN yarn install --production
EXPOSE 4040

CMD yarn start-build
